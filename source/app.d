import std.stdio;
import std.string;
import std.conv;
import vibe.http.server;
import vibe.http.router;
import vibe.core.core;
import vibe.core.log;
import vibe.data.json;
import vibe.http.client;
import core.time;
import mongodb;

Database db;

void main()
{
	db = new Database("memories", "", "");

	auto router = new URLRouter;
	router
		.post("/insertMemo",&insertMemo)
		.post("/getMemories",&getMemories)
		.post("/getMemo",&getMemo)
		.post("/deleteMemo",&deleteMemo)
		.post("/updateMemoName",&updateMemoName)
		.get("/getAll",&getAll)
		.post("/updateMemoDescription",&updateMemoDescription);
	auto l = listenHTTP("0.0.0.0:7821", router);
	runApplication();
	scope (exit) {
	    exitEventLoop(true);
	    sleep(1.msecs);
    }
    l.stopListening();
}

void insertMemo(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	auto tags = req_end.json["tags"];
	for(int i = 0; i < tags.length; i++)
	{
		if(db["tags"].find("tag", tags[i].to!string).length == 0)
		{
			Json tag = Json.emptyObject;
			tag["tag"] = tags[i];
			db["tags"].insert(tag);
		}
	}

	db["memo"].insert(req_end.json);
	res_end.writeJsonBody(["status": "ok"]);
}

void getMemories(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	Json memories = Json.emptyObject;
	memories["memories"] = db["memo"].find("user_id", req_end.json["user_id"].to!string);

	res_end.writeJsonBody(memories);
}

void getMemo(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	res_end.writeJsonBody(db["memo"].findOne("_id", req_end.json["memo_id"].to!string));
}

void deleteMemo(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	db["memo"].remove("_id", req_end.json["memo_id"].to!string);
	res_end.writeJsonBody(["status": "ok"]);
}

void updateMemoName(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	db["memo"].update("_id", req_end.json["memo_id"].to!string, "name", req_end.json["name"].to!string);
	res_end.writeJsonBody(["status": "ok"]);
}

void updateMemoDescription(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	db["memo"].update("_id", req_end.json["memo_id"].to!string, "description", req_end.json["description"].to!string);
	res_end.writeJsonBody(["status": "ok"]);
}

void getAll(HTTPServerRequest req_end, HTTPServerResponse res_end)
{
	res_end.writeJsonBody(db["memo"].find("", ""));
}